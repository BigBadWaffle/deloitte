# L2 Page Action Bar

## Installation
`npm i -s l2_page_"action_bar`

## Dependencies
None

## Example usage
Placed at the top of a `singlePage` dashboard:

``` json
{
    "traits": [
        "@l2_page_action_bar/index"
    ],
    "wgts": [
        {
            "traits": [
                {
                    "trait": "@l2_buttons/visual/primary/index",
                    "traitPrps": {
                        "cpt": "Create",
                        "icon": "add_box",
                        "enabled": false
                    }
                }
            ]
        },
        {
            "traits": [
                {
                    "trait": "@l2_buttons/visual/primary/index",
                    "traitPrps": {
                        "cpt": "Delete",
                        "icon": "delete",
                        "enabled": false
                    }
                }
            ]
        }
    ]
}
```
