# L2 Modal Panel

## Installation
`npm i -s l2_modal_panel`

## Dependencies
None

## Example usage
``` json
{
	"traits": [
		{
			"trait": "@l2_modal_panel/index",
			"traitPrps": {
				"minHeight": "500px",
				"autoResize": true,
				"wgtsMiddleRelId": "form",
				"containerPrps": {
					"flex": true,
					"padding": true
				},
				"wgtsTop": [
					{
						"traits": [
							"./visual/header/index"
						]
					}
				],
				"wgtsMiddle": [
					{
						"relId": "form",
						"traits": [
							"./visual/form/index"
						]
					}
				],
				"wgtsBottom": [
					{
						"traits": [
							"./visual/toolbar/index"
						]
					}
				]
			}
		}
	]
}
```
