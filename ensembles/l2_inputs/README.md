# Introduction 
This ensemble provides checkboxes, inputs and radio buttons for use in forms

# Usage
1. `npm i -s l2_inputs`
1. Open your index.json file
2. Add traits referencing `"@l2_inputs/..."`
