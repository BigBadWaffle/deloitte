# L2 Top Bar

## Note
This ensemble should normally not be used directly. Instead it is utilized by the `@l2_shell` ensemble.

## Installation
`npm i -s l2_topbar`

## Dependencies
None

## Example usage
``` json
{
	"traits": [
		"@l2_topbar/index"
	],
	"prps": {
		"flows": [
			{
				"from": "sidebar",
				"fromKey": "selectedDashboardData",
				"fromSubKey": "title",
				"toKey": "title"
			},
			{
				"from": "sidebar",
				"fromKey": "selectedDashboardData",
				"fromSubKey": "icon",
				"toKey": "icon"
			}
		]
	}	
}
```
