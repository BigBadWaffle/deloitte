# L2 Context Menu

## Installation
`npm i -s l2_buttons`

## Dependencies
None

## Theming
Copy the themes from the `theme` folder into your application's `theme` folder and override as required

## Example usage
``` json
{
	"relId": "btnClear",
	"traits": [
		{
			"trait": "@l2_buttons/visual/primary/index",
			"traitPrps": {
				"cpt": "DELETE",
				"icon": "delete"
			}
		},
		{
			"trait": "./functional/deleteRecord",
			"traitPrps": {}
		}
	]
}
```