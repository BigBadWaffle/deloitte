# L2 Shell

## Description
This ensemble provides a shell for any new application

It includes:
* Login
* Main dashboard with sidebar
* Capability of opening dashboards in a viewport
* Top bar to display currently open dashboard, logged in user and a log out button

## Installation
`npm i -s l2_shell`

## Dependencies
* `npm i -s l2_buttons`
* `npm i -s l2_inputs`
* `npm i -s l2_sidebar`
* `npm i -s l2_topbar`

## Theming
Copy the themes from the `theme` folder into your application's `theme` folder and override as required

## Example usage
This ensemble will modify your startup page to automatically use `@l2_shell/index`
