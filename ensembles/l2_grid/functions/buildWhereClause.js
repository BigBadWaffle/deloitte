const whereClause = $filters$
	.map(({ field, value, exactMatch }) => {
		let operator = '=';
		let useValue = value;

		if (!exactMatch) {
			operator = 'LIKE';
			useValue = `%${useValue}%`;
		}

		if (typeof(value) === 'string')
			useValue  = `'${useValue}'`;

		const result = `${field} ${operator} ${useValue}`;

		return result;
	})
	.join(' AND ');

console.log(whereClause);

whereClause;
