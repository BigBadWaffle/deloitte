# L2 Date Picker

## Installation
`npm i -s l2_date_picker`

## Dependencies
None

## Example usage
``` json
{
	"traits": [
		{
			"trait": "@l2_date_picker/index",
			"traitPrps": {
				"placeholder": "Select a date...",
				"value": null
			}
		}
	]
}
```
