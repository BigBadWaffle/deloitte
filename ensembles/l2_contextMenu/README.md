# L2 Context Menu

## Installation
`npm i -s l2_context_menu`

## Dependencies
None

## Example usage
``` json
{
	"type": "repeater",
	"prps": {
		"data": [
			{
				"name": "Record 1",
				"canDelete": false
			},
			{
				"name": "Record 2",
				"canDelete": true
			}
		],
		"rowMda": {
			"type": "container",
			"traits": [
				{
					"trait": "@l2_context_menu/index",
					"traitPrps": {
						"wgts": [
							{
								"condition": {
									"operator": "isTruthy",
									"value": "{{rowData.canDelete}}"
								},
								"traits": [
									{
										"trait": "@l2_context_menu/visual/menuItem",
										"traitPrps": {
											"cpt": "Delete Record: ((rowData.name))",
											"icon": "delete"
										}
									}
								],
								"prps": {
									"fireScript": {
										"actions": [
											{
												"type": "setState",
												"target": "CONTEXT1",
												"key": "display",
												"value": false
											},
											{
												"comment": "Some delete action using {{rowData}}"
											}
										]
									}
								}
							},
							{
								"traits": [
									{
										"trait": "@l2_context_menu/visual/menuItem",
										"traitPrps": {
											"cpt": "Cancel",
											"icon": "close"
										}
									}
								],
								"prps": {
									"fireScript": {
										"actions": [
											{
												"type": "setState",
												"target": "CONTEXT1",
												"key": "display",
												"value": false
											}
										]
									}
								}
							}
						]
					}
				}	
			],
			"wgts": [
				{
					"type": "label",
					"prps": {
						"cpt": "((rowData.name))"
					}
				}
			]
		}
	}
}
```
