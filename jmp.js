const { resolve } = require('path');
const { readdir, readFile, writeFile } = require('fs').promises;

const ignoreFolders = [
	'.git'
];

async function* getFiles(dir, fullPath = '') {
	const dirents = await readdir(dir, { withFileTypes: true });
	for (const dirent of dirents) {
		const res = resolve(dir, dirent.name);

		if (dirent.isDirectory()) {
			if (ignoreFolders.includes(dirent.name))
				continue;

			yield* getFiles(res);
		} else if (res.split('.').pop() !== 'json')
			continue;
		else {
			if (res.includes('mdaPackage') || res.includes('package.json'))
				continue;

			yield res;
		}
	}
}

;(async () => {
	console.log('Packaging...');
	const res = {};

	let cwd = `${process.cwd()}`;
	if (cwd.includes('\\'))
		cwd += '\\';
	else
		cwd += '/';

	const ensembleNames = Object.keys(JSON.parse(await readFile('package.json', 'utf-8')).dependencies);

	for await (const name of getFiles('.')) {
		const file = (await readFile(name, 'utf-8'))
			.replaceAll('\r', '')
			.replaceAll('\n', '')
			.replaceAll('\t', '');

		let accessor = res;

		const dirs = name.replace(cwd, '').split(/\/|\\/);
		const key = dirs.pop();

		dirs.forEach(d => {
			if (!accessor[d])
				accessor[d] = {};

			accessor = accessor[d];
		})

		let json;

		try {
			json = JSON.parse(file);
		} catch (e) {
			json = {
				type: 'label',
				prps: {
					cpt: `'${name}' is not valid JSON`
				}
			};
		}

		accessor[key] = json;
	}

	const indexJson = res.dashboard['index.json'];

	const ensembles = res.ensembles;
	if (ensembles) {
		Object.entries(ensembles).forEach(([k, v]) => {
			if (k.indexOf('l2_') === -1)
				return;

			res.dashboard['@' + k] = v;

			const config = v['config.json'];
			if (!config)
				return;

			if (config.themes) {
				config.themes.forEach(t => {
					if (!indexJson.themes.includes[t])
						indexJson.themes.push(t);

					const themeFileName = t + '.json';
					const theme = v.theme[themeFileName];

					const existingTheme = res.theme[themeFileName];

					if (!existingTheme)
						res.theme[themeFileName] = theme;
					else {
						Object.entries(theme).forEach(([k, v]) => {
							if (existingTheme[k] === undefined)
								existingTheme[k] = v;
						});
					}
				});
			}
		});
	}

	delete res.node_modules;
	delete res.ensembles;
	delete res['package.json'];
	delete res['package-lock.json'];
	delete res['serve.json'];

	const functions = res.theme['functions.json'];

	if (functions) {
		const entries = Object.entries(functions);
		for (let [k, v] of entries) {
			if (v.fn?.[0] !== '>')
				continue

			const f = `${v.fn.substr(1)}.js`;
			
			const convertedFileString = (await readFile(f, 'utf-8'))
				.replaceAll('\r', ' ')
				.replaceAll('\n', ' ')
				.replaceAll('\t', ' ');

			functions[k].fn = convertedFileString;
		}
	}

	await writeFile('packaged/mdaPackage.json', JSON.stringify(res));

	console.log('...completed');
})()